package gryazin.interview;

import java.util.Scanner;

public class OhYeah {

	/**
	 * In this class I'll implement all n^2 methods for the interview,
	 * that'll come to my head
	 * 
	 * my results:
	input: 3037000499
	g_1:9223372030926249001 1551
	g_2:9223372030926249001 2116
	
	g_3 works with max input ~10470 before StackOverflow appears
	 */
	public static void main(String[] args) {
		//Just showing off, there was not a word in the task about the length of n
		//let the maximum long be 2^63-1
		final long INPUT_MAX = 3037000499L;
		Scanner sc = new Scanner(System.in); 
		long n; 
		System.out.println("Please, input a natural number:");
		
		if(sc.hasNextLong()){
			n = sc.nextLong();
			if(n<=0) System.out.println("Natural number is an integer more than 0");
			else if(n>INPUT_MAX) System.out.println("Your number is too big to compute its square");
			else{//means all is ok
				long lStart = System.currentTimeMillis();
				long res = g_1(n);
				long lTime = System.currentTimeMillis()-lStart;
				System.out.println("g_1:" + res + " " + lTime + "ms");
				
				lStart = System.currentTimeMillis();
				res = g_2(n);
				lTime = System.currentTimeMillis()-lStart;
				System.out.println("g_2:" + res + " " + lTime + "ms");
				
				lStart = System.currentTimeMillis();
				res = g_3(n);
				lTime = System.currentTimeMillis()-lStart;
				System.out.println("g_3:" + res + " " + lTime + "ms");
			}
		}
		else{
			System.out.println("Wrong input");
		}
	}
	
	//Stupid sum += n;
	static long g_1(long n){
		long result = 0;
		for(long i = 0; i<n; i++){
			result += n;
		}
		return result;
	}
	
	//Stupid level 2:D sum(1.. n)*2 - n
	static long g_2(long n){
		long result = 0;
		for(long i = 1; i<=n; i++){
			result += i;
		}
		return result+=result - n;
	}
	
	//Recursive based on g_2 = sum(1.. n)*2 - n = 1 + 3 + 5 + .. + (2n-1)
	static long g_3(long n){
		return recursive(2*n-1);
	}
	
	static long recursive(long n){
		if(n>0) return recursive(n-2) + n;
		else return 0;
	}
}
